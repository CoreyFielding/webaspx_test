from openpyxl import workbook, load_workbook;

# load workbook
wb = load_workbook('webaspx.xlsx')

# load active worksheet (Sheet1)
sheet = wb.active

# maximum rows in sheet
max_row = sheet.max_row

# column to be populated (Column E)
target_col = 5

#loop through all rows in worksheet
for i in range(1, max_row+1):
    insert = sheet.cell(row=i, column=5).value = "Fictional Town"

# save workbook
wb.save('webaspx.xlsx')
